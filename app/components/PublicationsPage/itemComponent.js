import React, { Component } from 'react'

export default class ItemComponent extends Component {
    render () {
        const {item, addable, ...props} = this.props
        return (
            <div className={`search-result-holder ${!addable ? 'notAddable' : ''}`} {...(addable ? props : {})}>
                <div className='search-result'>{item.title}</div>
                {addable && <div className="add">Add</div>}
            </div>
        )
    }
}
