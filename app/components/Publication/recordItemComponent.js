import React, { Component } from 'react'

export default class RecordItemComponent extends Component {
    render () {
        const {item, addable, ...props} = this.props
        const title = item.title.title

        return (
            <div className={`record-search-result-holder ${!addable ? 'notAddable' : ''}`} {...(addable ? props : {})}>
                <div className='record-search-result'>
                    {title || item.doi || 'Error retrieving metadata'}
                </div>
                {addable && <div className="add">Add</div>}
            </div>
        )
    }
}