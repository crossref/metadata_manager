import React, { Component } from 'react'
import is from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import _ from 'lodash'
import {routes} from '../routing'

import { SET_AUTH_BEARER, login, resetLogin, resetPublications } from '../actions/application'



const mapStateToProps = (state) => ({
  loginState: state.login
})

const mapDispatchToProps = (dispatch) => bindActionCreators({
  asyncLogin: login,
  reduxResetLogin: resetLogin,
  reduxResetPublications: resetPublications
}, dispatch)

function NoRolesWarning(props) {
    return (
      <div className='login'>
          <div className='warn-card'>
                <span className='title'>Crossref Login</span>
                <div className='content'>
                    <span className='warning'>No role linked to your account</span>
                    <span><a href='#' onClick={props.back}><img src={`${routes.images}/Login/back.png`} /></a></span>
                    <span className='username'>{props.user}</span>
                    <span>There are no roles linked to this account.</span>
                    <span>Contact Crossref Support at <a href='mailto:support@crossref.org'>support@crossref.org</a> or use the support contact form.</span>

                    <span>
                      <input
                        type='submit'
						onClick={() => window.open('https://www.crossref.org/contact/')}
                        className='button-anchor loginButton'
                        value='Contact support' />
						</span>
                </div>
          </div>
      </div>
    )
}


@connect(mapStateToProps, mapDispatchToProps)
export default class LoginPage extends Component {

  static propTypes = {
    loginState: is.object.isRequired,
    asyncLogin: is.func.isRequired,
    reduxResetLogin: is.func.isRequired,
    reduxResetPublications: is.func.isRequired
  }

  state = {
    username: '',
    password: '',
    role: null,
    roles: []
  }

  componentDidMount () {
    sessionStorage.setItem('auth', '');
    this.props.reduxResetLogin()
    this.props.reduxResetPublications()
  }

  onSubmit = (e) => {
    e.preventDefault()
    this.props.asyncLogin(
      this.state.username,
      this.state.password,
      this.state.role ? this.state.role : _.get(this.props,'loginState.roles[0]',null)
    )
  }

  inputHandler = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  render () {
    const error = this.props.loginState && this.props.loginState.error
    const roles = this.props.loginState && this.props.loginState.roles
    const success = this.props.loginState && this.props.loginState.success

    if (success==true)
        return (<></>)

    if (roles != undefined)
    {
        if (roles.length==1) {
          this.props.asyncLogin(
          this.state.username,
          this.state.password,
          roles[0])
          return (<></>)
        } else if (roles.length==0) {
          return (<NoRolesWarning user={this.state.username} back={this.props.reduxResetLogin} />)
        }
    }

    return (
      <div className='login'>
      <div  className='Sunset'>
      Metadata Manager is being sunset in 2023. Read our blog post to <a href='https://www.crossref.org/blog/next-steps-for-content-registration/'>learn more</a>.
      </div>
        <div className={'login-card' + (error ? ' invalid-credentials' : '')}>
          {error && <div className='invalid-credentials' dangerouslySetInnerHTML={{__html: error}}></div>}
          <form onSubmit={this.onSubmit}>
            <span className='left-indent-16 card-title'>Login</span>
            {roles ?
             (<label>
               <span className='left-indent-16'>Please choose a role:</span>
                 <select id='cars' name='role' onChange={this.inputHandler}>
                   {_.map(roles, x => <option key={x} value={x}>{x}</option>)}
                 </select>
             </label>)
             : <>
               <label>
                 <span>Username</span>
                 <input
                   type='text'
                   name='username'
                   value={this.state.username}
                   onChange={this.inputHandler} />
                 {error && <span className='invalid-credentials'>Please enter valid email address or username</span>}
               </label>
               <label>
                 <span>Password</span>
                 <input
                   type='password'
                   name='password'
                   value={this.state.password}
                   onChange={this.inputHandler} />
                 {error && <span className='invalid-credentials'>Please enter valid password</span>}
               </label>
               </>
               }
            <input
              type='submit'
              className='button-anchor loginButton'
              value='Log in' />
            <div className='forgot-password'>
              <a href='https://authenticator.crossref.org/reset-password/'>Forgot password?</a>
            </div>
          </form>
        </div>
        <br/> <br/> <br/>
      </div>
    )
  }
}

