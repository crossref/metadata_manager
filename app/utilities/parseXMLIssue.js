import React from 'react'
import _ from 'lodash'

import {objectSearch, xmldoc} from './helpers'


const parseXMLIssue = function (xml, ownerPrefix = '') {
  let retObj = {showSection: false}


  const parsedIssue = xmldoc(xml)
  const journal_issue = objectSearch(parsedIssue, 'journal_issue')
  const journal_volume = objectSearch(parsedIssue, 'journal_volume')

  if (journal_volume) {
    delete journal_issue['journal_volume']
    var theVolume = objectSearch(journal_volume, 'volume') || ''
    var volumeDoiData = objectSearch(journal_volume, 'doi_data') || ''
    var volumeDoi = objectSearch(volumeDoiData, 'doi') || ''
    var volumeUrl = objectSearch(volumeDoiData, 'resource') || 'http://'
  }

  const issueTitle = objectSearch(journal_issue, 'title') || ''
  const issue = objectSearch(journal_issue, 'issue') || ''
  const issueDoi = objectSearch(journal_issue, 'doi') || ''
  const issueUrl = objectSearch(journal_issue, 'resource') || 'http://'
  const special_numbering = objectSearch(parsedIssue, 'special_numbering') || ''
  let publication_date = objectSearch(journal_issue, 'publication_date')

  if(!Array.isArray(publication_date)) publication_date = [publication_date] //Code below wants array of values, but if we accept only 1 date, we get only 1 object, so we transform into array

  const onlinePubDate = _.find(publication_date, (pubDate) => {
    if (pubDate) {
      if (pubDate['-media_type'] === 'online') {
        return pubDate
      }
    }
  })

  const printPubDate = _.find(publication_date, (pubDate) => {
    if (pubDate) {
      if (pubDate['-media_type'] === 'print') {
        return pubDate
      }
    }
  })
  var printDateYear = ''
  var printDateMonth = ''
  var printDateDay = ''
  if (printPubDate) {
    printDateYear = printPubDate['year'] ? printPubDate['year'] : ''
    printDateMonth = printPubDate['month'] ? printPubDate['month'] : ''
    printDateDay = printPubDate['day'] ? printPubDate['day'] : ''
  }

  var onlineDateYear = ''
  var onlineDateMonth = ''
  var onlineDateDay = ''
  if (onlinePubDate) {
    onlineDateYear = onlinePubDate['year'] ? onlinePubDate['year'] : ''
    onlineDateMonth = onlinePubDate['month'] ? onlinePubDate['month'] : ''
    onlineDateDay = onlinePubDate['day'] ? onlinePubDate['day'] : ''
  }

  const archiveLocations = objectSearch(parsedIssue, 'archive_locations')
  var archive = ''
  if (archiveLocations) {
    archive = archiveLocations.archive['-name']
  }

  const setIssue = {
    issue: issue,
    issueTitle: issueTitle,
    issueDoi: issueDoi || `${ownerPrefix ? `${ownerPrefix}/` : ''}`,
    issueUrl: issueUrl || 'http://',
    printDateYear: printDateYear,
    printDateMonth: printDateMonth,
    printDateDay: printDateDay,
    onlineDateYear: onlineDateYear,
    onlineDateMonth: onlineDateMonth,
    onlineDateDay: onlineDateDay,
    archiveLocation: archive,
    specialIssueNumber: special_numbering,
    volume: theVolume || '',
    volumeDoi: volumeDoi || `${ownerPrefix}/`,
    volumeUrl: volumeUrl || 'http://'
  }

  // contributor loading
  const contributors = objectSearch(parsedIssue, 'contributors')
  var contributee = []
  // contributors are divied into 2 types
  // person_name and organization
  var person_name = undefined
  if (contributors) {
    retObj.showSection = true
    person_name = objectSearch(contributors, 'person_name')

    if (person_name) { // if exist
      var pnameArray
      if (!Array.isArray(person_name)) {
         pnameArray = [person_name]
      } else { 
        pnameArray=person_name
      }// its an array

      _.each(pnameArray, (person) => {
        var altName=''
        // to allow reading of previous bad XML
        if(person_name['alt-name']){
          if (person_name['alt-name'].name){
            altName=person_name['alt-name'].name 
          }else{
            altName=person_name['alt-name']
          }
        }

        contributee.push(
          {
            firstName: person.given_name ? person.given_name : '',
            lastName: person.surname ? person.surname : '',
            suffix: person.suffix ? person.suffix : '',
            affiliation: person.affiliation ? person.affiliation : '',
            orcid: person.ORCID ? person.ORCID : '',
            alternativeName:altName,
            role: person['-contributor_role'] ? person['-contributor_role'] : '',
            errors: {}
          }
        )
      })
    }
  }

  if (contributee.length <= 0) {
    retObj.showSection = false
    contributee.push(
      {
        firstName: '',
        lastName: '',
        suffix: '',
        affiliation: '',
        orcid: '',
        role: '',
        alternativeName: '',
        errors: {}
      }
    )
  }

  retObj = _.extend(retObj, {
      issue: setIssue,
      optionalIssueInfo: contributee
  })

  return retObj
}
export default parseXMLIssue